package net.astropayments.nbank.payments.repositories;


import net.astropayments.nbank.payments.models.transaction.Transaction;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TransactionsRepository extends MongoRepository<Transaction, ObjectId> {
    Transaction findByIdAndStatus(ObjectId transactionId, Integer status);

    Transaction findByGateIdAndStatus(ObjectId gateId, Integer status);

    List<Transaction> findAllByStatus(Integer status);
}
