package net.astropayments.nbank.payments.controller;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.dto.MerchantOperDto;
import net.astropayments.nbank.payments.exception.NotFoundException;
import net.astropayments.nbank.payments.models.Answer;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperResponseData;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.repositories.GatesRepository;
import net.astropayments.nbank.payments.services.NBService;
import net.astropayments.nbank.payments.services.SystemService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequiredArgsConstructor
public class NBController {
    private final SystemService systemService;
    private final NBService nbService;
    private final GatesRepository gatesRepository;

    @PostMapping("/api/account/add")
    private Callable<Answer> accountAdd(Long amount, String currency, String account) {
        return () -> systemService.accountAdd(Balance.builder()
                .amount(amount)
                .currency(currency)
                .build(), account).get();
    }

    @GetMapping("/api/reqMerchantOper")
    public Callable<ReqMerchantOperResponseData> getMerchantOper(MerchantOperDto operDto) {
        final var gate = gatesRepository.findByName(operDto.getGateName());

        if (gate != null) {
            return () -> nbService.merchantOper(operDto, gate).get();
        } else {
            throw new NotFoundException("Гейт не найден!");
        }
    }
}
