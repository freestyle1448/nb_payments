package net.astropayments.nbank.payments.services;

import net.astropayments.nbank.payments.models.Answer;
import net.astropayments.nbank.payments.models.transaction.Balance;

import java.util.concurrent.CompletableFuture;

public interface SystemService {
    CompletableFuture<Answer> accountAdd(Balance balance, String account);
}
