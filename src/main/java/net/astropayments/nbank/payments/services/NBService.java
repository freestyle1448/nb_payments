package net.astropayments.nbank.payments.services;

import net.astropayments.nbank.payments.dto.MerchantOperDto;
import net.astropayments.nbank.payments.models.Gate;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperResponseData;
import net.astropayments.nbank.payments.models.transaction.Transaction;

import java.util.concurrent.CompletableFuture;

public interface NBService {
    void withdraw(Transaction transaction, Gate gate);

    void checkTransaction(Transaction transaction, Gate gate);

    void declineTransaction(Transaction transaction, String errorCause, String systemError);

    CompletableFuture<ReqMerchantOperResponseData> merchantOper(MerchantOperDto operDto, Gate gate);

    void ping();
}
