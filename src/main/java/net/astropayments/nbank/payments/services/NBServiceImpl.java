package net.astropayments.nbank.payments.services;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.dto.MerchantOperDto;
import net.astropayments.nbank.payments.models.Gate;
import net.astropayments.nbank.payments.models.nb.Person;
import net.astropayments.nbank.payments.models.nb.reqacctocard.AccToCard;
import net.astropayments.nbank.payments.models.nb.reqacctocard.AccToCardData;
import net.astropayments.nbank.payments.models.nb.reqacctocard.ReqAccToCardData;
import net.astropayments.nbank.payments.models.nb.reqavaliablerest.ReqAvaliableRest;
import net.astropayments.nbank.payments.models.nb.reqavaliablerest.SignedData;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOper;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperData;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperResponse;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperResponseData;
import net.astropayments.nbank.payments.models.nb.requeststatus.RequestStatus;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static net.astropayments.nbank.payments.models.transaction.Status.DENIED;
import static net.astropayments.nbank.payments.models.transaction.Status.WAITING;

@Service
@Async
@RequiredArgsConstructor
public class NBServiceImpl implements NBService {
    private static final Logger logger = LoggerFactory.getLogger(NBServiceImpl.class);

    private final RetryTemplate retryTemplate;
    private final PlatformTransactionManager transactionManager;
    private final ManualRepository manualRepository;
    private final NBHTTPSRequest nbHTTPSRequest;
    private final NBResponseHandler nbResponseHandler;

    @Override
    public void withdraw(Transaction transaction, Gate gate) {
        logger.info("Обработка транзакции на вывод");
        if (transaction.getStage() != 3) {
            Object result = null;
            try {
                result = retryTemplate.execute(context -> accountSub(transaction));
            } catch (Exception ex) {
                logger.error("Списание с аккаунта при выводе не удалось!", ex);

                transaction.setStatus(WAITING);
                transaction.setStage(0);

                nbResponseHandler.saveTransaction(transaction);
            }

            if (result == null) {
                logger.error("При попытке списания средств с аккаунта произошла ошибка!\n {}", transaction);
                return;
            }

            logger.info("Списание с аккаунта успешно");
        }

        AccToCard request = AccToCard.builder()
                .nbCredentials(gate.getNbCredentials())
                .reqId(transaction.getTransactionNumber().toString())
                .data(AccToCardData.builder()
                        .accNo(transaction.getSenderCredentials().getAccount())
                        .reqData(ReqAccToCardData.builder()
                                .cardNo(transaction.getReceiverCredentials().getCardNumber())
                                .amount(transaction.getAmount().getAmount().doubleValue() / 100)
                                .currency(gate.getBalance().getCurrency())
                                .build())
                        .build())
                .build();

        nbHTTPSRequest.reqAccToCard(request)
                .whenComplete((response, throwable) -> {
                    if (response != null && throwable == null) {
                        nbResponseHandler.handleWithdraw(response, transaction);
                    } else {
                        declineTransaction(transaction, "Ошибка при  проведении платежа", "Ошибка при получении ответа от NB");

                        logger.info("Списание средств неуспешно для транзакции - {}, сообщение - Не удалось получить ответ от NB"
                                , transaction);
                    }
                });
    }

    @Override
    public void checkTransaction(Transaction transaction, Gate gate) {
        RequestStatus request = new RequestStatus(gate.getNbCredentials().getCustomerId(),
                transaction.getTransactionNumber().toString(),
                UUID.randomUUID().toString());

        nbHTTPSRequest.requestStatus(request)
                .whenComplete((response, throwable) -> {
                    if (response != null && throwable == null) {
                        nbResponseHandler.handleStatus(response, transaction);
                    }
                });
    }

    @Override
    public void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        nbResponseHandler.declineTransaction(transaction, errorCause, systemError);
    }

    @Override
    public CompletableFuture<ReqMerchantOperResponseData> merchantOper(MerchantOperDto operDto, Gate gate) {
        return nbHTTPSRequest.reqMerchantOper(ReqMerchantOper.builder()
                .nbCredentials(gate.getNbCredentials())
                .signedDataObject(ReqMerchantOperData.builder()
                        .amount((double) (operDto.getAmount() / 100))
                        .currencyCode(operDto.getCurrency())
                        .dbCr(operDto.getDbCr())
                        .maskCardNo(operDto.getMaskCardNo())
                        .merchantAuthCode(operDto.getMerchantAuthCode())
                        .payer(Person.builder()
                                .lastName(operDto.getPayerLastName())
                                .build())
                        .pcOrderNo(operDto.getPcOrderNo())
                        .rrn(operDto.getRrn())
                        .receiverInn(operDto.getReceiverInn())
                        .receiverName(operDto.getReceiverName())
                        .strAccount(operDto.getStrAccount())
                        .terminalCode(operDto.getTerminalCode())
                        .url(operDto.getUrl())
                        .build())
                .build())
                .thenApply(ReqMerchantOperResponse::getData);
    }

    @Override
    public void ping() {
        final var requests = new ArrayList<ReqAvaliableRest>();
        final var gates = manualRepository.findNBGates();

        for (Gate gate : gates) {
            final var nbCredentials = gate.getNbCredentials();
            ReqAvaliableRest requestPing = ReqAvaliableRest.builder()
                    .signedDataObject(new SignedData(nbCredentials.getAccount()))
                    .customerId(nbCredentials.getCustomerId())
                    .nbCredentials(nbCredentials)
                    .gateId(gate.getId())
                    .reqId(UUID.randomUUID().toString())
                    .build();

            requests.add(requestPing);
        }

        for (ReqAvaliableRest request : requests) {
            nbHTTPSRequest.ping(request).whenComplete((response, throwable) -> {
                if (response != null && throwable == null) {
                    nbResponseHandler.handlePing(response);
                }
            });
        }
    }

    private Object accountSub(Transaction transaction) {
        var def = new DefaultTransactionDefinition();
        def.setName("forTr" + transaction.getTransactionNumber());
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            logger.info("Начало списания средств с аккаунта");
            final var senderAccount = manualRepository.findAndModifyAccountSub(transaction.getSenderCredentials().getAccount()
                    , transaction.getFinalAmount());
            if (senderAccount == null) {
                logger.error("Аккаунт с указанным ID не найден! Для транзакции {}", transaction);

                transactionManager.rollback(status);

                transaction.setStatus(DENIED);
                transaction.setStatus(2);
                transaction.setSystemError("Аккаунт отправителя не найден!");
                transaction.setErrorReason("Аккаунт отправителя не найден!");

                nbResponseHandler.saveTransaction(transaction);

                return null;
            }
            if (senderAccount.getBalance().getAmount() < 0) {
                logger.error("Недостаточно средств на аккаунте отправителя! Для транзакции {}", transaction);

                transactionManager.rollback(status);

                transaction.setStatus(DENIED);
                transaction.setStatus(2);
                transaction.setSystemError("INSUFFICIENT_FUNDS");
                transaction.setErrorReason("INSUFFICIENT_FUNDS");

                nbResponseHandler.saveTransaction(transaction);

                return null;
            }

        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("Ошибка при списании с аккаунта в выводе", ex);
            throw ex;
        }

        logger.info("Коммитим списание");
        transactionManager.commit(status);
        return new Object();
    }
}
