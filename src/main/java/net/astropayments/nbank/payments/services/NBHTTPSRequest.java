package net.astropayments.nbank.payments.services;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.astropayments.nbank.payments.models.NbCredentials;
import net.astropayments.nbank.payments.models.RunnerLog;
import net.astropayments.nbank.payments.models.nb.TokenHolder;
import net.astropayments.nbank.payments.models.nb.auth.Auth;
import net.astropayments.nbank.payments.models.nb.auth.AuthResponse;
import net.astropayments.nbank.payments.models.nb.reqacctocard.AccToCard;
import net.astropayments.nbank.payments.models.nb.reqacctocard.AccToCardResponse;
import net.astropayments.nbank.payments.models.nb.reqacctocard.ResponseData;
import net.astropayments.nbank.payments.models.nb.reqavaliablerest.ReqAvaliableRest;
import net.astropayments.nbank.payments.models.nb.reqavaliablerest.ReqAvaliableRestResponse;
import net.astropayments.nbank.payments.models.nb.reqavaliablerest.ResponseRestData;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOper;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperResponse;
import net.astropayments.nbank.payments.models.nb.reqmerchantoper.ReqMerchantOperResponseData;
import net.astropayments.nbank.payments.models.nb.requeststatus.RequestStatus;
import net.astropayments.nbank.payments.models.nb.requeststatus.StatusResponse;
import net.astropayments.nbank.payments.repositories.RunnerLogsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.FileReader;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static net.astropayments.nbank.payments.models.RequestTypes.*;

@Component
@ConfigurationProperties("nb")
@RequiredArgsConstructor
public class NBHTTPSRequest {
    private static final String LOG_MES = "Запрос - {}\nОтвет - {}\n";
    private static final Logger logger = LoggerFactory.getLogger(NBHTTPSRequest.class);
    private static final ObjectMapper jsonMapper = new JsonMapper();

    private final TokenHolder token;
    private final RunnerLogsRepository runnerLogsRepository;
    private final RestTemplate restTemplate;
    private String url;
    private String filePath;

    public CompletableFuture<AccToCardResponse> reqAccToCard(AccToCard request) {
        String dataAsString;
        try {
            dataAsString = jsonMapper.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            logger.error("Ошибка при парсинге запроса\n", e);

            return CompletableFuture.completedFuture(null);
        }

        String signedData = Base64.getEncoder().encodeToString(dataAsString.getBytes());

        request.setSignedData(signedData);
        request.setSign(createSign(dataAsString));
        request.setToken(getToken(request.getNbCredentials()));

        AccToCardResponse response = null;

        try {
            response = restTemplate.postForObject(url + "/ReqAccToCard", request, AccToCardResponse.class);
        } catch (Exception ex) {
            logger.error("При отправке запроса (withdraw) к NB возникла ошибка! ", ex);

            return CompletableFuture.completedFuture(null);
        } finally {
            runnerLogsRepository.save(RunnerLog.builder()
                    .date(new Date())
                    .requestType(WITHDRAW)
                    .request(request.toString())
                    .response(response == null ? null : response.toString())
                    .build());
        }

        if (response != null) {
            byte[] decodedBytes = Base64.getDecoder().decode(response.getSignedData());
            String decodedString = new String(decodedBytes);

            try {
                response.setResponseData(jsonMapper.readValue(decodedString, ResponseData.class));
            } catch (JsonProcessingException e) {
                logger.error("Ошибка при парсинге ответа от NB\n", e);

                return CompletableFuture.completedFuture(null);
            }
        }
        logger.info(LOG_MES, request, response);
        return CompletableFuture.completedFuture(response);
    }

    public CompletableFuture<StatusResponse> requestStatus(RequestStatus request) {
        StatusResponse response = null;

        try {
            response = restTemplate.postForObject(url + "/RequestStatus", request, StatusResponse.class);
        } catch (Exception ex) {
            logger.error("При отправке запроса (check) к NB возникла ошибка! ", ex);

            return CompletableFuture.completedFuture(null);
        } finally {
            runnerLogsRepository.save(RunnerLog.builder()
                    .date(new Date())
                    .requestType(CHECK)
                    .request(request.toString())
                    .response(response == null ? null : response.toString())
                    .build());
        }

        logger.info(LOG_MES, request, response);
        return CompletableFuture.completedFuture(response);
    }

    public CompletableFuture<ReqAvaliableRestResponse> ping(ReqAvaliableRest request) {

        String dataAsString;
        try {
            dataAsString = jsonMapper.writeValueAsString(request.getSignedDataObject());
        } catch (JsonProcessingException e) {
            logger.error("Ошибка при парсинге запроса\n", e);

            return CompletableFuture.completedFuture(null);
        }
        String signedData = Base64.getEncoder().encodeToString(dataAsString.getBytes());

        request.setSignedData(signedData);
        request.setSign(createSign(dataAsString));
        request.setToken(getToken(request.getNbCredentials()));

        ReqAvaliableRestResponse response = null;
        try {
            response = restTemplate.postForObject(url + "/ReqAvaliableRest", request, ReqAvaliableRestResponse.class);
        } catch (Exception ex) {
            logger.error("При отправке запроса (ping) к NB возникла ошибка! ", ex);

            return CompletableFuture.completedFuture(null);
        } finally {
            runnerLogsRepository.save(RunnerLog.builder()
                    .date(new Date())
                    .requestType(PING)
                    .request(request.toString())
                    .response(response == null ? null : response.toString())
                    .build());
        }

        logger.info(LOG_MES, request, response);

        if (response != null) {
            response.setGateId(request.getGateId());
            byte[] decodedBytes = Base64.getDecoder().decode(response.getSignedData());
            String decodedString = new String(decodedBytes);

            try {
                response.setData(jsonMapper.readValue(decodedString, ResponseRestData.class));
            } catch (JsonProcessingException e) {
                logger.error("Ошибка при парсинге запроса\n", e);

                return CompletableFuture.completedFuture(null);
            }
        }

        return CompletableFuture.completedFuture(response);
    }

    public CompletableFuture<ReqMerchantOperResponse> reqMerchantOper(ReqMerchantOper request) {
        String dataAsString;
        try {
            dataAsString = jsonMapper.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            logger.error("Ошибка при парсинге запроса\n", e);

            return CompletableFuture.completedFuture(null);
        }

        String signedData = Base64.getEncoder().encodeToString(dataAsString.getBytes());

        request.setSignedData(signedData);
        request.setSign(createSign(dataAsString));
        request.setToken(getToken(request.getNbCredentials()));

        ReqMerchantOperResponse response = null;

        try {
            response = restTemplate.postForObject(url + "/ReqAccToCard", request, ReqMerchantOperResponse.class);
        } catch (Exception ex) {
            logger.error("При отправке запроса (merchantOper) к NB возникла ошибка! ", ex);

            return CompletableFuture.completedFuture(null);
        } finally {
            runnerLogsRepository.save(RunnerLog.builder()
                    .date(new Date())
                    .requestType(WITHDRAW)
                    .request(request.toString())
                    .response(response == null ? null : response.toString())
                    .build());
        }

        if (response != null) {
            byte[] decodedBytes = Base64.getDecoder().decode(response.getSignedData());
            String decodedString = new String(decodedBytes);

            try {
                response.setData(jsonMapper.readValue(decodedString, ReqMerchantOperResponseData.class));
            } catch (JsonProcessingException e) {
                logger.error("Ошибка при парсинге ответа от NB\n", e);

                return CompletableFuture.completedFuture(null);
            }
        }
        logger.info(LOG_MES, request, response);
        return CompletableFuture.completedFuture(response);
    }

    private synchronized String getToken(NbCredentials nbCredentials) {
        if (token.isExpire()) {
            token.setAuthResponse(reqAuth(nbCredentials));
        }

        return token.getAuthResponse().getToken();
    }

    private synchronized AuthResponse reqAuth(NbCredentials nbCredentials) {
        Auth auth = new Auth(nbCredentials.getCustomerId(), nbCredentials.getPassword(), UUID.randomUUID().toString());

        AuthResponse authResponse = null;
        try {
            authResponse = restTemplate.postForObject(url + "/ReqAuth", auth, AuthResponse.class);
        } catch (Exception ex) {
            logger.error("При отправке запроса (auth) к NB возникла ошибка! ", ex);
        } finally {
            runnerLogsRepository.save(RunnerLog.builder()
                    .date(new Date())
                    .requestType(AUTH)
                    .request(auth.toString())
                    .response(authResponse == null ? null : authResponse.toString())
                    .build());
        }

        logger.info(LOG_MES, auth, authResponse);
        return authResponse;
    }

    @SneakyThrows
    private String createSign(String data) {
        java.security.Security.addProvider(
                new org.bouncycastle.jce.provider.BouncyCastleProvider()
        );

        StringBuilder pkcs8Lines = new StringBuilder();
        try (BufferedReader rdr = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = rdr.readLine()) != null) {
                pkcs8Lines.append(line);
            }
        }
        String pkcs8Pem = pkcs8Lines.toString();
        pkcs8Pem = pkcs8Pem.replace("-----BEGIN RSA PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replace("-----END RSA PRIVATE KEY-----", "");
        pkcs8Pem = pkcs8Pem.replaceAll("\\s+", "");

        byte[] pkcs8EncodedBytes = Base64.getDecoder().decode(pkcs8Pem);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PrivateKey privKey = kf.generatePrivate(keySpec);
        Signature md5withrsa = Signature.getInstance("MD5withRSA");
        md5withrsa.initSign(privKey);

        md5withrsa.update(data.getBytes());
        byte[] signature = md5withrsa.sign();

        return Base64.getEncoder().encodeToString(signature);
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
