package net.astropayments.nbank.payments.services;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.nb.NBStatuses;
import net.astropayments.nbank.payments.models.nb.reqacctocard.AccToCardResponse;
import net.astropayments.nbank.payments.models.nb.reqavaliablerest.ReqAvaliableRestResponse;
import net.astropayments.nbank.payments.models.nb.requeststatus.StatusResponse;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.models.transaction.Transaction;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.Date;

import static net.astropayments.nbank.payments.models.transaction.Status.*;

@Component
@RequiredArgsConstructor
public class NBResponseHandler {
    private static final String BAD_MES = "Списание средств неуспешно для транзакции - {}, статус - {}, сообщение - {}";
    private static final String BANK_DECLINED = "Платеж отклонён банком";
    private static final Logger logger = LoggerFactory.getLogger(NBServiceImpl.class);

    private final RetryTemplate retryTemplate;
    private final PlatformTransactionManager transactionManager;
    private final ManualRepository manualRepository;

    public void handlePing(ReqAvaliableRestResponse response) {
        if (response.getData().getErrCode() == null) {
            manualRepository.findAndModifyGateSet(response.getGateId(), Balance
                    .builder()
                    .amount((long) (response.getData().getRest() * 100))
                    .build());
        }
    }

    public void handleWithdraw(AccToCardResponse response, Transaction transaction) {
        if (response.getResponseData() != null) {
            final var message = response.getResponseData().getErrText();
            final var status = response.getResponseData().getResponseCode();

            switch (checkNBStatus(response.getResponseData().getResponseText().toUpperCase())) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, status, message);
                    declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case TOCHECK:
                    transaction.setStage(3);
                    transaction.setStatus(IN_PROCESS);
                    saveTransaction(transaction);
                    break;
                case CHECK:
                    logger.info("Заявка на списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    transaction.setStatus(IN_PROCESS);
                    transaction.setStage(1);
                    saveTransaction(transaction);
                    break;
                case ACCEPTED:
                    confirmTransaction(transaction);

                    logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    break;
            }
        } else {
            logger.info(BAD_MES
                    , transaction, 0, "timeout");
            declineTransaction(transaction, BANK_DECLINED, "timeout");
        }
    }

    public void handleStatus(StatusResponse response, Transaction transaction) {
        if (response != null) {
            final var message = response.getErrText();
            final var status = response.getErrCode();

            switch (checkNBStatus(response.getStatus().toUpperCase())) {
                case DECLINED:
                    logger.info(BAD_MES
                            , transaction, status, message);
                    declineTransaction(transaction, BANK_DECLINED, message);
                    break;
                case TOCHECK:
                    transaction.setStage(3);
                    transaction.setStatus(IN_PROCESS);
                    saveTransaction(transaction);
                    break;
                case CHECK:
                    logger.info("Заявка на списание средств ждёт выполнения для транзакции - {}, статус - {}"
                            , transaction, status);
                    transaction.setStatus(IN_PROCESS);
                    transaction.setStage(1);
                    saveTransaction(transaction);
                    break;
                case ACCEPTED:
                    confirmTransaction(transaction);

                    logger.info("Списание средств успешно для транзакции - {}, статус - {}"
                            , transaction, status);
                    break;
            }
        }
    }

    private NBStatuses checkNBStatus(String responseText) {
        if (responseText.equals("NEW") || responseText.equals("INPROCESS")
                || responseText.equals("PCPROCEED")) {
            return NBStatuses.CHECK;
        }
        if (responseText.equals("PCDECLINED") || responseText.equals("DECLINED")) {
            return NBStatuses.DECLINED;
        }
        if (responseText.equals("TOCHECK")) {
            return NBStatuses.TOCHECK;
        }

        if (responseText.equals("PROCEED")) {
            return NBStatuses.ACCEPTED;
        }

        return NBStatuses.DECLINED;
    }

    private void confirmTransaction(Transaction transaction) {
        if (transaction != null) {
            transaction.setStatus(SUCCESS);
            transaction.setStage(2);

            saveTransaction(transaction);
            logger.info("Транзакция успешно принята(confirmTransaction) - {}"
                    , transaction);
        } else {
            logger.error("Для подтверждения транзакции передали не существующую транзакцию");
        }
    }

    void declineTransaction(Transaction transaction, String errorCause, String systemError) {
        if (transaction != null) {
            final var result = retryTemplate.execute(context -> {
                var def = new DefaultTransactionDefinition();
                def.setName("forTr" + transaction.getTransactionNumber());
                def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

                TransactionStatus status = transactionManager.getTransaction(def);
                try {
                    if ((manualRepository.findAndModifyAccountAdd(transaction.getSenderCredentials().getAccount(), transaction.getFinalAmount()) == null)) {
                        logger.error("Во время отмены транзакции аккаунт не был найден! - {}"
                                , transaction);

                        transactionManager.rollback(status);
                        return null;
                    } else {
                        transactionManager.commit(status);

                        return new Object();
                    }
                } catch (Exception ex) {
                    transactionManager.rollback(status);
                    logger.error("Ошибка при возврате средств", ex);
                    throw ex;
                }
            });

            if (result != null) {
                transaction.setStatus(DENIED);
                transaction.setStage(1);
                transaction.setErrorReason(errorCause);
                transaction.setSystemError(systemError);
                saveTransaction(transaction);

                logger.error("Успешный возврат! - {}", transaction);
            }
        } else {
            logger.error("Для отмены транзакции передали не существующую транзакцию");
        }
    }

    public void saveTransaction(Transaction transaction) {
        final int status = transaction.getStatus();
        final var stage = transaction.getStage();
        final var errorReason = transaction.getErrorReason();
        final var systemError = transaction.getSystemError();

        if (status == DENIED || status == SUCCESS) {
            transaction.setEndDate(new Date());
            if (status == DENIED || status == MANUAL) {
                transaction.setErrorReason(errorReason);
                transaction.setSystemError(systemError);
            }
        }
        transaction.setStatus(status);
        transaction.setStage(stage);

        manualRepository.saveTransaction(transaction);
    }
}
