package net.astropayments.nbank.payments.services;

import lombok.RequiredArgsConstructor;
import net.astropayments.nbank.payments.models.Account;
import net.astropayments.nbank.payments.models.Answer;
import net.astropayments.nbank.payments.models.transaction.Balance;
import net.astropayments.nbank.payments.repositories.ManualRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.concurrent.CompletableFuture;

@Service
@Async
@RequiredArgsConstructor
public class SystemServiceImpl implements SystemService {
    private static final Logger logger = LoggerFactory.getLogger(NBServiceImpl.class);

    private final RetryTemplate retryTemplate;
    private final PlatformTransactionManager transactionManager;
    private final ManualRepository manualRepository;

    @Override
    public CompletableFuture<Answer> accountAdd(Balance balance, String account) {
        logger.info("Обработка операции на ввод");

        Account result;
        try {
            result = retryTemplate.execute((RetryCallback<Account, Throwable>) context -> accountAddTr(balance, account));
        } catch (Throwable ex) {
            logger.error("Пополнение не удалось!", ex);

            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("Пополнение не удалось!" + ex.getLocalizedMessage())
                    .build());
        }

        if (result == null) {
            logger.error("При попытке пополнении аккаунта произошла ошибка!\n");
            return CompletableFuture.completedFuture(Answer.builder()
                    .status("ERR")
                    .err("При попытке пополнении аккаунта произошла ошибка!\n")
                    .build());
        }

        logger.info("Пополнение аккаунта успешно");

        return CompletableFuture.completedFuture(Answer.builder()
                .status("OK")
                .amount(result.getBalance().getAmount())
                .build());
    }

    private Account accountAddTr(Balance balance, String account) {
        var def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = transactionManager.getTransaction(def);
        Account receiverAccount;
        try {
            logger.info("Начало списания средств с аккаунта");
            receiverAccount = manualRepository.findAndModifyAccountAdd(account
                    , balance);
            if (receiverAccount == null) {
                logger.error("Аккаунт с указанным ID не найден! ");

                transactionManager.rollback(status);

                return null;
            }

        } catch (Exception ex) {
            transactionManager.rollback(status);
            logger.error("Ошибка при пополнении аккаунта ", ex);
            throw ex;
        }

        logger.info("Коммитим пополнение");
        transactionManager.commit(status);

        return receiverAccount;
    }
}
