package net.astropayments.nbank.payments.models.nb.reqacctocard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AccToCardResponse {
    @JsonProperty("CustomerId")
    private final String customerId;
    @JsonProperty("ReqId")
    private final String reqId;
    @JsonProperty("Token")
    private final String token;
    @JsonProperty("SignedData")
    private final String signedData;
    @JsonProperty("Sign")
    private final String sign;

    @JsonIgnore
    private ResponseData responseData;
}
