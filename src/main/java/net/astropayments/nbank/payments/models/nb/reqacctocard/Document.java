package net.astropayments.nbank.payments.models.nb.reqacctocard;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Document {
    @JsonProperty("IdentityType")
    private String identityType;
    @JsonProperty("IdentityId")
    private String identityId;
    @JsonProperty("IdentitySeries")
    private String identitySeries;
    @JsonProperty("Country")
    private String country;
    @JsonProperty("DocDate")
    private String docDate;
    @JsonProperty("ExpDate")
    private String expDate;
    @JsonProperty("Nationality")
    private String nationality;
}
