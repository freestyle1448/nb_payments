package net.astropayments.nbank.payments.models.nb.reqavaliablerest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResponseRestData {
    @JsonProperty("Status")
    private String status;
    @JsonProperty("Rest")
    private Double rest;
    @JsonProperty("ErrCode")
    private String errCode;
    @JsonProperty("ErrText")
    private String errText;
}
