package net.astropayments.nbank.payments.models.nb.reqacctocard;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.nb.Person;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReqAccToCardData {
    @JsonProperty("Amount")
    private Double amount;
    @JsonProperty("Currency")
    private String currency;
    @JsonProperty("CardNo")
    private String cardNo;
    @JsonProperty("Receiver")
    private Person receiver;
    @JsonProperty("Sender")
    private Person sender;
}
