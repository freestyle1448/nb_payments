package net.astropayments.nbank.payments.models.nb.reqavaliablerest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SignedData {
    @JsonProperty("StrAccount")
    private final String strAccount;
}
