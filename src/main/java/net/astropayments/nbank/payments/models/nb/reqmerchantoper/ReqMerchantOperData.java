package net.astropayments.nbank.payments.models.nb.reqmerchantoper;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.nb.Person;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReqMerchantOperData {
    @JsonProperty("StrAccount")
    private String strAccount;
    @JsonProperty("ReceiverName")
    private String receiverName;
    @JsonProperty("ReceiverInn")
    private String receiverInn;
    @JsonProperty("Payer")
    private Person payer;
    private String url;
    @JsonProperty("DateTransact")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy", timezone = "Europe/Moscow")
    private Date dateTransact;
    @JsonProperty("Amount")
    private Double amount;
    @JsonProperty("MerchantAuthCode")
    private String merchantAuthCode;
    @JsonProperty("DbCr")
    private Integer dbCr;
    @JsonProperty("RRN")
    private String rrn;
    @JsonProperty("CurrencyCode")
    private String currencyCode;
    @JsonProperty("FiscalReceipt")
    private FiscalData fiscalReceipt;
    @JsonProperty("TerminalCode")
    private String terminalCode;
    @JsonProperty("MaskCardNo")
    private String maskCardNo;
    @JsonProperty("PcOrderNo")
    private String pcOrderNo;
}
