package net.astropayments.nbank.payments.models.nb;

public enum NBStatuses {
    CHECK,
    DECLINED,
    ACCEPTED,
    TOCHECK
}
