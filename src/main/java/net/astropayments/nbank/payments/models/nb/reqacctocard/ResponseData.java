package net.astropayments.nbank.payments.models.nb.reqacctocard;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseData {
    @JsonProperty("ResponseCode")
    private String responseCode;
    @JsonProperty("ResponseText")
    private String responseText;
    @JsonProperty("AuthCode")
    private String authCode;
    @JsonProperty("RRN")
    private String rrn;
    @JsonProperty("ErrCode")
    private String errCode;
    @JsonProperty("ErrText")
    private String errText;
    @JsonProperty("Status")
    private String status;
}
