package net.astropayments.nbank.payments.models.nb.reqavaliablerest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.bson.types.ObjectId;

@Data
public class ReqAvaliableRestResponse {
    @JsonProperty("RespId")
    private final String respId;
    @JsonProperty("SignedData")
    private final String signedData;

    @JsonIgnore
    private ObjectId gateId;
    @JsonIgnore
    private ResponseRestData data;
}
