package net.astropayments.nbank.payments.models.nb.requeststatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import net.astropayments.nbank.payments.models.NbCredentials;

@Data
public class RequestStatus {
    @JsonProperty("CustomerId")
    private final String customerId;
    @JsonProperty("RequestId")
    private final String requestId;
    @JsonProperty("ReqId")
    private final String reqId;

    @JsonIgnore
    private NbCredentials nbCredentials;
}
