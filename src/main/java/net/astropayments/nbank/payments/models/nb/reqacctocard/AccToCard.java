package net.astropayments.nbank.payments.models.nb.reqacctocard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.NbCredentials;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccToCard {
    @JsonProperty("CustomerId")
    private String customerId;
    @JsonProperty("ReqId")
    private String reqId;
    @JsonProperty("Token")
    private String token;
    @JsonProperty("SignedData")
    private String signedData;
    @JsonProperty("Sign")
    private String sign;

    @JsonIgnore
    private AccToCardData data;
    @JsonIgnore
    private NbCredentials nbCredentials;
}
