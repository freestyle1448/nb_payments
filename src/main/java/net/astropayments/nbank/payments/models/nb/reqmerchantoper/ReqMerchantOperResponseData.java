package net.astropayments.nbank.payments.models.nb.reqmerchantoper;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReqMerchantOperResponseData {
    private String status;
    private String responseText;
}
