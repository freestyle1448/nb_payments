package net.astropayments.nbank.payments.models.nb;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.astropayments.nbank.payments.models.nb.reqacctocard.Address;
import net.astropayments.nbank.payments.models.nb.reqacctocard.Document;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person {
    @JsonProperty("FirstName")
    private String firstName;
    @JsonProperty("MiddleName")
    private String middleName;
    @JsonProperty("LastName")
    private String lastName;
    @JsonProperty("PostAddress")
    private Address postAddress;
    @JsonProperty("Phone")
    private String phone;
    @JsonProperty("BirthDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    private Date birthDate;
    @JsonProperty("Identity")
    private Document identity;
    @JsonProperty("CountryOfBirth")
    private String countryOfBirth;
}
