package net.astropayments.nbank.payments.models.nb.reqacctocard;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccToCardData {
    @JsonProperty("AccNo")
    private String accNo;
    @JsonProperty("ReqData")
    private ReqAccToCardData reqData;
}
