package net.astropayments.nbank.payments.models.nb.requeststatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class StatusResponse {
    @JsonProperty("RespId")
    private final String respId;
    @JsonProperty("RequestId")
    private final String requestId;
    @JsonProperty("ErrCode")
    private final String errCode;
    @JsonProperty("ErrText")
    private final String errText;
    @JsonProperty("RespText")
    private final String respText;
    @JsonProperty("Status")
    private final String status;
}
