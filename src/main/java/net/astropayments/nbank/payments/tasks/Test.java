package net.astropayments.nbank.payments.tasks;

import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;

@RestController
public class Test {
    /*private final NBHTTPSRequest nbhttpsRequest;
    ObjectMapper jsonMapper = new JsonMapper();

    public Test(NBHTTPSRequest nbhttpsRequest) {
        this.nbhttpsRequest = nbhttpsRequest;
    }

    @SneakyThrows
    @GetMapping("/")
    public void test() {
        final var s = "MIIJfgYJKoZIhvcNAQcCoIIJbzCCCWsCAQExDjAMBggqhQMHAQECAgUAMHEGCSqGSIb3DQEHAa" +
                "BkBGJ7IlJlc3BvbnNlQ29kZSI6bnVsbCwiUmVzcG9uc2VUZXh0IjpudWxsLCJBdXRoQ29kZSI6bnVsbCwi" +
                "UlJOIjpudWxsLCJFcnJDb2RlIjpudWxsLCJFcnJUZXh0IjpudWxsfaCCBhgwggYUMIIFgKADAgECAhA+e" +
                "44A7qgqiE0ZqoakZrlmMAoGCCqFAwcBAQMDMIG3MSAwHgYJKoZIhvcNAQkBFhFjcGNhQGNyeXB0b" +
                "3Byby5ydTELMAkGA1UEBhMCUlUxFTATBgNVBAgMDNCc0L7RgdC60LLQsDEVMBMGA1UEBwwM0" +
                "JzQvtGB0LrQstCwMSUwIwYDVQQKDBzQntCe0J4gItCa0KDQmNCf0KLQni3Qn9Cg0J4iMTEwLwYDV" +
                "QQDDCjQo9CmINCa0KDQmNCf0KLQni3Qn9Cg0J4gKNCT0J7QodCiIDIwMTIpMB4XDTE4MDUyODA" +
                "4Mjg0NloXDTIzMDUyODA4Mzg0NlowggEKMRwwGgYJKoZIhvcNAQkBFg1zYkBuYi1iYW5rLnJ1MQsw" +
                "CQYDVQQGEwJSVTFFMEMGA1UECAw80JrQsNGA0LDRh9Cw0LXQstC+LdCn0LXRgNC60LXRgdG" +
                "B0LrQsNGPINGA0LXRgdC/0YPQsdC70LjQutCwMRkwFwYDVQQHDBDQp9C10YDQutC10YHRgdC6" +
                "MScwJQYDVQQKDB7QkNCeINCd0LDRgNC+0LTQvdGL0Lkg0LHQsNC90LoxDzANBgNVBAsMBlJOM" +
                "DFHRzFBMD8GA1UEAww40JrQsNGA0LDRg9C70L7QsiDQkNC90LTRgNC10Lkg0JDQu9C10LrRgdC" +
                "w0L3QtNGA0L7QstC40YcwZjAfBggqhQMHAQEBATATBgcqhQMCAiQABggqhQMHAQECAgNDAARAs" +
                "nXuufPRMTKfq4arZ7NWQ0llhdUVjmp5VxAdn+HrNB3uane0iYF1kqX5ZdLIXltzGLlq6HDlGL3qs0W48zg" +
                "+KKOCAwkwggMFMA4GA1UdDwEB/wQEAwID6DA0BgkrBgEEAYI3FQcEJzAlBh0qhQMCAjIBCYezkG" +
                "qH/5FGhaGXGISr8xvlMYOsAQIBAQIBADAdBgNVHQ4EFgQUfiCYc9ZYMKmiN3KoDHTjpnSx1vEwJQY" +
                "DVR0lBB4wHAYIKwYBBQUHAwIGCCsGAQUFBwMEBgYqhQMGDQEwKwYDVR0QBCQwIoAPMjAxO" +
                "DA1MjgwODI4NDVagQ8yMDE5MDUyODA4Mjg0NVowga4GA1UdHwSBpjCBozBOoEygSoZIaHR0cDo" +
                "vL2NkcC5jcnlwdG9wcm8ucnUvY2RwLzA2ZDg0OTA0NjAwYjYzNDBjMDFmYzYzNjg1NjNiMDk2MzhlM" +
                "DRhOWIuY3JsMFGgT6BNhktodHRwOi8vY3BjYTIwLmNyeXB0b3Byby5ydS9jZHAvMDZkODQ5MDQ2M" +
                "DBiNjM0MGMwMWZjNjM2ODU2M2IwOTYzOGUwNGE5Yi5jcmwwgaEGCCsGAQUFBwEBBIGUMIGR" +
                "MDYGCCsGAQUFBzABhipodHRwOi8vb2NzcC5jcnlwdG9wcm8ucnUvb2NzcDIwMTIvb2NzcC5zcmYwV" +
                "wYIKwYBBQUHMAKGS2h0dHA6Ly9jcGNhMjAuY3J5cHRvcHJvLnJ1L2FpYS8wNmQ4NDkwNDYwMGI" +
                "2MzQwYzAxZmM2MzY4NTYzYjA5NjM4ZTA0YTliLmNydDCB9AYDVR0jBIHsMIHpgBQG2EkEYAtjQMA" +
                "fxjaFY7CWOOBKm6GBvaSBujCBtzEgMB4GCSqGSIb3DQEJARYRY3BjYUBjcnlwdG9wcm8ucnUxCzA" +
                "JBgNVBAYTAlJVMRUwEwYDVQQIDAzQnNC+0YHQutCy0LAxFTATBgNVBAcMDNCc0L7RgdC60LLQ" +
                "sDElMCMGA1UECgwc0J7QntCeICLQmtCg0JjQn9Ci0J4t0J/QoNCeIjExMC8GA1UEAwwo0KPQpiDQmt" +
                "Cg0JjQn9Ci0J4t0J/QoNCeICjQk9Ce0KHQoiAyMDEyKYIRAN0QTuSUkMKA5xHBD3hxvpswCgYIKoUD" +
                "BwEBAwMDgYEADWmzoWYxY9slsjPlm3IC7oV+kYrmHrPN7SfsV/zzeiw+iAF9xEqwRUgGwd/zMiR57rx" +
                "oOI3ZLKM3WDt8oVUfTs93rTVWVsAxh2vHLjCN51UitmNaaASfqNy1OQ+j1qcphfvyxaWwG3KemhagT8" +
                "8SDHFLPWYM4eD8gOTP0g78szAxggLFMIICwQIBATCBzDCBtzEgMB4GCSqGSIb3DQEJARYRY3BjY" +
                "UBjcnlwdG9wcm8ucnUxCzAJBgNVBAYTAlJVMRUwEwYDVQQIDAzQnNC+0YHQutCy0LAxFTATBgNV" +
                "BAcMDNCc0L7RgdC60LLQsDElMCMGA1UECgwc0J7QntCeICLQmtCg0JjQn9Ci0J4t0J/QoNCeIjExMC" +
                "8GA1UEAwwo0KPQpiDQmtCg0JjQn9Ci0J4t0J/QoNCeICjQk9Ce0KHQoiAyMDEyKQIQPnuOAO6oKoh" +
                "NGaqGpGa5ZjAMBggqhQMHAQECAgUAoIIBjTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGC" +
                "SqGSIb3DQEJBTEPFw0xODA5MTcxMTE5NTRaMC8GCSqGSIb3DQEJBDEiBCBTz64PN1TJ/mV60ubj" +
                "lf/Ssgv7KoHTIXsI7IcNB6kbeDCCASAGCyqGSIb3DQEJEAIvMYIBDzCCAQswggEHMIIBAzAKBggqhQ" +
                "MHAQECAgQgWRnySyaVWOj9DAGKu5JDAeMGrz64RgTL2i6Kvn6ERFswgdIwgb2kgbowgbcxIDAeBg" +
                "kqhkiG9w0BCQEWEWNwY2FAY3J5cHRvcHJvLnJ1MQswCQYDVQQGEwJSVTEVMBMGA1UECAwM0JzQvtGB0LrQstCwMRUwEwYDVQQHDAzQnNC+0YHQutCy0LAxJTAjBgNVBAoMHNCe0J7QniAi0JrQ" +
                "oNCY0J/QotCeLdCf0KDQniIxMTAvBgNVBAMMKNCj0KYg0JrQoNCY0J/QotCeLdCf0KDQniAo0JPQntC" +
                "h0KIgMjAxMikCED57jgDuqCqITRmqhqRmuWYwDAYIKoUDBwEBAQEFAARAC7u8ZfQfBxIQbBt2mRs" +
                "DhRM1PUQHduPgcBWXhvnLq56PQCGUxDP44Iiu0h+R0e6Vicho/X4Bh24T/suW8/JUvw==";

        byte[] decodedBytes = Base64.getDecoder().decode(s);
        String decodedString = new String(decodedBytes);
        final var substring = decodedString.substring(59, decodedString.length() - 2019);
        final var accToCardResponse = jsonMapper.readValue(substring, ResponseData.class);
        System.out.println(decodedString);
        System.out.println(accToCardResponse);
*//*        final var build = AccToCardData.builder()
                .accNo("30109810800000000001")
                .reqData(ReqAccToCardData.builder()
                        .receiver(Person.builder()
                                .postAddress(Address.builder()
                                        .street("Проектируемый проект 2364")
                                        .city("МОСКВА")
                                        .stateCode("RU")
                                        .country("РФ")
                                        .postalCode("101000")
                                        .build())
                                .phone("79031112233")
                                .identity(Document.builder()
                                        .identityType("Паспорт РФ")
                                        .identityId("123456")
                                        .identitySeries("4600")
                                        .country(null)
                                        .nationality("РОССИЙСКАЯ ФЕДЕРАЦИЯ")
                                        .expDate("23.11.2020")
                                        .docDate("11.09.2010")
                                        .build())
                                .countryOfBirth("Российская Федерация")
                                .firstName("ИВАН")
                                .middleName("ПЕТРОВИЧ")
                                .lastName("СИДОРОВ")
                                .birthDate(new Date(1970, 12, 21))
                                .build())
                        .sender(Person.builder()
                                .postAddress(Address.builder()
                                        .street("Проектируемый проект 6423")
                                        .city("МОСКВА")
                                        .stateCode("RU")
                                        .country("РФ")
                                        .postalCode("101000")
                                        .build())
                                .phone("79851112233")
                                .identity(Document.builder()
                                        .identityType("Паспорт РФ")
                                        .identityId("654321")
                                        .identitySeries("4500")
                                        .country(null)
                                        .nationality("РОССИЙСКАЯ ФЕДЕРАЦИЯ")
                                        .expDate("23.11.2026")
                                        .docDate("11.09.2010")
                                        .build())
                                .countryOfBirth("Российская Федерация")
                                .firstName("СИДОР")
                                .middleName("ИВАНОВИЧ")
                                .lastName("ПЕТРОВ")
                                .birthDate(new Date(1982, 10, 01))
                                .build())
                        .amount(1.1)
                        .currency("810")
                        .cardNo("5547817050124672")
                        .build())
                .build();

        nbhttpsRequest.reqAccToCard(AccToCard.builder()
                .data(build)
                .gateCredentials(GateCredentials.builder()
                        .customerId("e4479421-ab3c-40ad-89d3-8eeec42b56c7")
                        .password("mQkFT!Sv")
                        .build())
                .build());*//*
    }*/

}
